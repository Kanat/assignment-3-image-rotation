#ifndef STATUS
#define STATUS
#include <stdbool.h>
#include <stdio.h>

enum close_status {
    CLOSE_OK,
    CLOSE_ERROR
};
enum open_status {
    OPEN_OK,
    OPEN_ERROR
};

#endif
