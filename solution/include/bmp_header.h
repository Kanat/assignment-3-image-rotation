#ifndef BMP_HEADER
#define BMP_HEADER

#include "image.h"
#include  <stdint.h>

enum read_status get_header(FILE* in, struct bmp_header *header);

struct bmp_header create_default_header(void) ;

struct bmp_header create_header(const struct image *image);
#endif
