#ifndef FILE_H
#define FILE_H

#include "file_status.h"
#include <stdio.h>
enum close_status close_file(FILE *file); 

enum open_status open_file(FILE** file, char *filename, char* mode);

#endif
