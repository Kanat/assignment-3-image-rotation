#ifndef ROTATER
#define ROTATER
#include "image.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(const struct image *source );
#endif
