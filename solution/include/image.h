#ifndef ROTATE_IMAGE
#define ROTATE_IMAGE
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image {
  uint32_t width; 
  uint32_t height;
  struct pixel* data;
};

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image create_image(uint32_t height, uint32_t width);

void delete_image(struct image image);

#endif 
