#include "bmp.h"
#include "file.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Invalid parameters");
        return 1;
    }
    FILE* input = NULL; //link to a file
    const enum open_status input_stat = open_file(&input, argv[1], "rb");

    if (input == NULL || input_stat == OPEN_ERROR) {
        fprintf(stderr, "Couldn't open file");
        return EXIT_FAILURE;
    }
    
    
    struct image image = {0};
    const enum read_status open_status = from_bmp(input, &image);

    if (open_status != READ_OK) {
        free(image.data);
        fprintf(stderr, "Couldn't open file");
        return EXIT_FAILURE;
    }   
    const enum close_status close_status = close_file(input);

    if (close_status != CLOSE_OK) {
        fprintf(stderr, "Couldn't close file");
        return 1;
    }

    FILE* output = NULL;
    const enum open_status output_stat = open_file(&output, argv[2], "wb");

    if (output == NULL || output_stat == OPEN_ERROR) {
        close_file(output);
        free(image.data);
        fprintf(stderr, "Couldn't open output file");
        return 1;
    }
    struct image result_image = rotate(&image);
    const enum write_status write_stat = to_bmp(output, &result_image);
    if (write_stat != WRITE_OK) {
        free(image.data);
        free(result_image.data);
        fprintf(stderr, "couldn't rotate an image");
        return 1;
    }

    const enum close_status close_stat = close_file(output);

    if (close_stat != CLOSE_OK) {
        free(image.data);
        free(result_image.data);
        fprintf(stderr, "Couldn't close output file");
        return -1;
    }

    free(result_image.data);
    free(image.data);
    return 0;
}
