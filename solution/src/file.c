#include "file_status.h"
#include "file.h"
#include <stdio.h>


enum close_status close_file(FILE *file) {
    if (fclose(file)) return CLOSE_ERROR;
    return CLOSE_OK;
}
enum open_status open_file(FILE** file, char *filename, char* mode){
    *file = fopen(filename, mode);
    if (*file) return OPEN_OK;
    return OPEN_ERROR;
}
