#include "rotate.h"

struct image rotate(const struct image *source) {
    struct image image = create_image(source->width, source->height); //try to change places with height and width
    struct pixel *data = image.data;
    for (size_t i = 0; i < source->height; ++i) {
        for (size_t j = 0; j < source->width; ++j) {
            data[source->height * (j + 1) - i - 1] = source->data[source->width * i + j];
        }
    }
    return image;
}
