#include "image.h"
#include <stdlib.h>

struct image create_image (uint32_t height, uint32_t width) {
    struct pixel *pixel = malloc(sizeof(struct pixel) * height * width);
    struct image image = (struct image) {.height = height, .width = width, .data = pixel};
    return image;
}

void delete_image(struct image image) {
    free(image.data);
}
