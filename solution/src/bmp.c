#include "bmp.h"
#include "bmp_header.h"
#include "image.h"
#include <stdio.h>


uint32_t get_padding(uint32_t width) {
    return (uint32_t) 4 - (width * sizeof(struct pixel) % 4);
}


enum read_status get_pixels(FILE* in, struct image* image) {
    const uint32_t width = image -> width;
    const uint32_t padding = get_padding(image -> width);
    struct pixel *const pixels = image->data;

    for (size_t i = 0; i < image->height; ++i) {
        size_t count = fread(width * i + pixels, sizeof(struct pixel), width, in);
        if (count != width || fseek(in, padding, SEEK_CUR) != 0) {
            // free(image->data);
            return READ_INVALID_BITS;
        }
    }
    
    return READ_OK;
}


enum read_status from_bmp(FILE* in, struct image *image) {
    struct bmp_header header = create_default_header();
    const enum read_status status = get_header(in, &header);

    if (status != READ_OK) {
        return status;
    }
    *image = create_image(header.biHeight, header.biWidth); 

    return get_pixels(in, image);
}

enum write_status write_header(FILE* out, const struct image *image) {
    struct bmp_header header = create_header(image);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        // free(image->data);
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status write_pixels(FILE* out, const struct image *const image) {
    const uint32_t width = image->width;
    const uint32_t padding = get_padding(width);

    for (size_t i = 0; i < image->height; ++i) {
        const size_t write_1 = fwrite(width * i + image->data, sizeof(struct pixel), width, out);
        const size_t write_2 = fwrite(image->data, sizeof(char), padding, out);
        if (write_1 != width || write_2 != padding) {
            // free(image->data); //free memory in case bad write
            return WRITE_ERROR;
        }
    }     
    return WRITE_OK; //in case everything is ok return WRITE_OK
}

enum write_status to_bmp(FILE* out, const struct image *image) {
    enum write_status status = write_header(out, image);
    if (status != WRITE_OK) {
        return WRITE_ERROR;
    }
    return write_pixels(out, image);
}

