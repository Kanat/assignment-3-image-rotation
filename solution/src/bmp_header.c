#include "bmp.h"
#include "bmp_header.h"
#include "image.h"
#include <stdio.h>

enum read_status get_header(FILE* in, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) { 
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

struct bmp_header create_default_header(void) {
    return (struct bmp_header) {
        .bfType = 0x4d42, //default type
        // .bfileSize = 0,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40, //default size
        // .biWidth = ,
        // .biHeight = ,
        .biPlanes = 1,
        .biBitCount = 24, //default
        .biCompression = 0,
        // .biSizeImage = ,
        .biXPelsPerMeter = 3400, //default
        .biYPelsPerMeter = 3400, //default
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

struct bmp_header create_header(const struct image *image) {
    struct bmp_header new_header = create_default_header();
    new_header.biWidth = image->width;
    new_header.biHeight = image->height;
    new_header.biSizeImage = sizeof(struct pixel) * image->height * image->width;
    new_header.bfileSize = new_header.biSizeImage + sizeof(struct bmp_header);
    return new_header;
}
